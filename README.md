workqueue-go
============

一个简单go语言实现的workqueue，类似于Linux内核中的workqueue
使用方法
-----
获取源码:<br>

    go get github.com/Ishukone/workqueue-go
代码例子：
```go
package main

import (
        "fmt"
        "github.com/Ishukone/workqueue-go"
)

func greeting(work *workqueue.Work) {
        fmt.Printf("hello, %s\n", work.Data)
}

func main() {
        wq := workqueue.CreateWorkQueue(4)
        var greet string

        for {
                fmt.Scan(&greet)

                work := new(workqueue.Work)
                work.Data = greet
                work.Action = greeting
                wq.ScheduleWork(work)
        }
}
```
build:<br>

    go build
test:<br>
运行build出的执行程序，在命令提示符中输入多个名字，名字之间用空格隔开，键入回车，每个名字将会被构造成一个work，然后由workqueue调度打印。